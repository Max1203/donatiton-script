package common;

public class Config {
    public static final String FOUND = "The Raptor Center Give to the Max 2020";
    public static final String DONATION_AMOUNT = "500";
    public static final String DONATION_MESSAGE = "I hope all your dreams will come true someday.";
    public static final String FIRST_NAME = "Max";
    public static final String LAST_NAME = "Bova";
    public static final String EMAIL_ADDRESS = "maksym.bova@gmail.com";
    public static final String DROP_DOWN_VALUE = "TARGET";
}
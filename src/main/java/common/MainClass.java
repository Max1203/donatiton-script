package common;

import org.apache.maven.shared.invoker.*;
import org.testng.TestListenerAdapter;
import org.testng.TestNG;
import java.io.File;
import java.util.Arrays;
import java.util.Collections;

public class MainClass {

    public static void main(String[] args) {



        InvocationRequest request = new DefaultInvocationRequest();
        request.setPomFile(new File("./pom.xml"));
        request.setGoals(Collections.singletonList("install"));
        Invoker invoker = new DefaultInvoker();
        invoker.setMavenHome(new File(System.getenv("MAVEN_HOME")));
        try {
            invoker.execute(request);
        } catch (MavenInvocationException e) {
            e.printStackTrace();
        }

        /*

        TestNG testNG = new TestNG();
        testNG.setTestSuites(Arrays.asList("testng.xml"));
        testNG.setPreserveOrder(true);
        testNG.run();

         */




    }
}

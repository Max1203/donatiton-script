package Tests;

import common.Utils;
import common.drivers.DriverFactory;
import org.junit.Assert;
import org.openqa.selenium.WebDriver;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Parameters;
import pages.*;

public class BaseTest {
    protected DonateToFoundPage donateToFoundPage;
    protected FoundPage foundPage;
    protected FundraiserSearchPage fundraiserSearchPage;
    protected HomePage homePage;
    protected MakeDonationPage makeDonationPage;

    private WebDriver driver;

    @BeforeMethod
    @Parameters("browser")
    public void setUp(String browser){
        DriverFactory driverFactory = new DriverFactory(browser);
        driver = driverFactory.getDriver();
        initPages(driver);
        homePage.openHomePage();
        Assert.assertTrue(homePage.atPage());
        homePage.closeAlert();
    }

    @AfterMethod
    public void tearDown(){
        Utils.sleep(5000);

        if (driver != null){
            driver.quit();
            driver = null;
        }
    }

    private void initPages(WebDriver driver){
        donateToFoundPage = new DonateToFoundPage(driver);
        foundPage = new FoundPage(driver);
        fundraiserSearchPage = new FundraiserSearchPage(driver);
        homePage = new HomePage(driver);
        makeDonationPage = new MakeDonationPage(driver);
    }
}

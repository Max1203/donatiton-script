package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class DonateToFoundPage extends BasePage {

    private static final By yourDonationTitle = By.xpath("//legend[text()='Your Donation']");
    private static final By donationAmountInputField = By.xpath("//input[@name='donationAmountInput']");
    private static final By donationMessageTextArea = By.xpath("//textarea[@name=\"donationMessage\"]");
    private static final By firstNameInputField = By.xpath("//input[@name='firstName']");
    private static final By lastNameInputField = By.xpath("//input[@name='lastName']");
    private static final By emailInputField = By.xpath("//input[@name='email']");
    private static final By donationPrivacyDropDown = By.xpath("//select[@name='donorNameVisibility']");
    private static final By donateWithCreditCardButton = By.xpath("//button[text()=' Donate with Credit Card']");
    private static final By fancyBox = By.xpath("//div[@class='fancybox-outer']");



    public DonateToFoundPage(WebDriver driver) {
        super(driver);
    }

    public boolean atPage() {
        waitUntilPageToLoad();
        return isWebElementPresent(yourDonationTitle);
    }

    public void fillDonationAmountInputField(String text){
        enterText(donationAmountInputField, text);
    }

    public void fillDonationMessageTextArea(String text){
        enterText(donationMessageTextArea, text);
    }

    public void fillFirstNameInputField(String text){
        enterText(firstNameInputField, text);
    }

    public void fillLastNameInputField(String text){
        enterText(lastNameInputField, text);
    }

    public void fillEmailInputField(String text){
        enterText(emailInputField, text);
    }

    public void selectFromDonationPrivacyDropDown(String value){
        selectFromDropDownByIndex(donationPrivacyDropDown, value);
    }

    public void clickDonateWithCreditCardButton(){
        click(donateWithCreditCardButton);
    }

    public boolean hasPaymentWindowShowedUp(){
        try {
            waitToBePresent(fancyBox);
            return true;
        }catch (Exception e){
            return false;
        }


    }

}

package pages;

import common.Config;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class FundraiserSearchPage extends BasePage {
    private static final By h1 = By.xpath("//h1[text()='Fundraiser Search']");
    private static final By item = By.xpath("//a[text()= 'The Raptor Center Give to the Max 2020']");

    public FundraiserSearchPage(WebDriver driver) {
        super(driver);
    }

    public boolean atPage() {
        waitUntilPageToLoad();
        return isWebElementPresent(h1);
    }

    public void clickFoundByName(String name){
        By found = By.xpath(String.format("//a[text()='%s']", name));
        waitToBeClickable(found);
        click(found);
    }
}

package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class FoundPage extends BasePage {

    private static final By participantTabs = By.xpath("//ul[@id='participantTabs']");
    private static final By donateButton = By.xpath("//a[@id='btn-support-participant-top']");

    public FoundPage(WebDriver driver) {
        super(driver);
    }

    public boolean atPage() {
        waitUntilPageToLoad();
        return isWebElementPresent(participantTabs);
    }

    public void clickDonateButton(){
        waitToBeClickable(donateButton);
        click(donateButton);
    }
}

package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class HomePage extends BasePage{
    private static final String link = "https://crowdfund.umn.edu/";
    private static final By donateNowButton = By.xpath("//div[@class=\"header__top wrap\"]//a[@id='btn-donate-now']");
    private static final By descriptionOnMainPage = By.xpath("//p[contains(text(), 'The University of Minnesota')]");

    public HomePage(WebDriver driver) {
        super(driver);
    }

    public boolean atPage() {
        waitUntilPageToLoad();
        return isWebElementPresent(descriptionOnMainPage);
    }

    public void openHomePage(){
        openPage(link);
        waitUntilPageToLoad();
    }

    public void clickDonateNowButton(){
        waitToBeClickable(donateNowButton);
        click(donateNowButton);
    }
}

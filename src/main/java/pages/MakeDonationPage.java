package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class MakeDonationPage extends BasePage{
    private static final By h1 = By.xpath("//h1[text()='Make a Donation']");
    private static final By searchField = By.xpath("//input[@id= 'searchterm']");
    private static final By searchButton = By.xpath("//input[@class='btn-form']");

    public MakeDonationPage(WebDriver driver) {
        super(driver);
    }

    public boolean atPage() {
        waitUntilPageToLoad();
        return isWebElementPresent(h1);
    }

    public void enterTextInSearchField(String text){
        waitToBeClickable(searchField);
        enterText(searchField, text);
    }

    public void clickSearchButton(){
        waitToBeClickable(searchButton);
        click(searchButton);
    }
}

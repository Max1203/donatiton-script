package pages;

import org.openqa.selenium.*;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import common.Utils;
import java.util.List;
import java.util.NoSuchElementException;

public abstract class BasePage {
    private WebDriver driver;

    public BasePage(WebDriver driver){
        this.driver = driver;
    }

    public abstract boolean atPage();


    public WebElement find(By locator){
        return driver.findElement(locator);
    }

    public List<WebElement> findAllElements(By locator){
        return driver.findElements(locator);
    }

    public void click(By locator){
        find(locator).click();
    }

    public void enterText(By locator, String text){
        WebElement element = find(locator);
        element.click();
        element.clear();
        element.sendKeys(text);

    }

    public void openPage(String link){
        driver.get(link);
    }

    public void waitToBeClickable(By locator, int time){
        WebDriverWait wait = new WebDriverWait(driver, time);
        wait.until(ExpectedConditions.elementToBeClickable(locator));
    }

    public void waitToBeClickable(By locator){
        waitToBeClickable(locator, 30);
    }

    public void waitToBePresent(By locator, int time){
        WebDriverWait wait = new WebDriverWait(driver, time);
        wait.until(ExpectedConditions.presenceOfElementLocated(locator));
    }

    public void waitToBePresent(By locator){
        waitToBePresent(locator, 30);
    }

    public void waitUntilPageToLoad(){
        ExpectedCondition<Boolean> expectation = new
                ExpectedCondition<Boolean>() {
                    public Boolean apply(WebDriver driver){
                        return ((JavascriptExecutor) driver).executeScript("return document.readyState").toString().equals("complete");
                    }
                };
        try {
            Utils.sleep(1000);
            WebDriverWait wait = new WebDriverWait(driver, 30);
            wait.until(expectation);
        } catch (Throwable error) {
            error.printStackTrace();
        }
    }

    public boolean isWebElementPresent(By locator){
        try{
            find(locator);
            return true;
        }catch (NoSuchElementException e){
            return false;
        }
    }

    public boolean isAlertPresent(){
        try{
            driver.switchTo().alert();
            return true;
        }catch (NoAlertPresentException e){
            return false;
        }
    }

    public void closeAlert(){
        if(isAlertPresent()){
            driver.switchTo().alert().accept();
        }
    }

    public void selectFromDropDownByIndex(By locator, String value){
        Select dropdown = new Select(find(locator));
        dropdown.selectByValue(value);

    }
}
